manga-js
========

An open source desktop manga reader in Javascript using React and Redux. All you need to do is paste in the manga URL
from a supported site (right now only mangareader.net) and manga-js will automatically parse the manga data from the site
and you can just read the manga from manga-js. This allows you to avoid all of the annoying pop-ups, unwanted redirects, or potentially malicious ads
from reading the manga on the manga site and also allow you to save your current progress or download the manga for offline reading
(manga downloading hasn't been implemented yet).

## Running

In order to build and run manga-js, you have to first have npm installed. Then run `npm install` in the project directory
and then `npm start` to start the electron application.

## Testing

To run the jest tests, run `npm test`.
